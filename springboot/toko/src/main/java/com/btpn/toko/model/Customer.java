package com.btpn.toko.model;

import lombok.*;
import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "customer")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long customerId;
    @Column(nullable = false)
    private String customerName;
    private String customerAddress;
    private String customerPhone;
    private boolean isActive = true;
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastOrder;
    private String pic;
}
