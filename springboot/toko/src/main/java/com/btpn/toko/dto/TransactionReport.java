package com.btpn.toko.dto;

import java.util.Date;


import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionReport {
    private Date orderDate;
    private String customerName;
    private String itemName;
    private Double price;
    private Integer quantity;
    private Double totalPrice;
}
