package com.btpn.toko.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;
import jakarta.persistence.*;

@Entity
@Table(name = "item")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long itemId;
    @Column(nullable = false)
    private String itemName;
    private Double price;
    private Integer stock;
    private Date lastRestock;
    private boolean isAvailable;
    private boolean isDeleted = false;
    @JsonProperty("isAvailable")
    public boolean isAvailable() {
        return isAvailable;
    }
}
